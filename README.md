# Open-RPC Client Generator

Generate clients from Open-RPC APIs.

## CLI

Client generator works with WebSocket and HTTP urls.

```shell
orpc --help
```
